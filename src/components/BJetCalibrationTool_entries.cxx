#include <BJetCalibrationTool/BJetCalibrationTool.h>
#include <BJetCalibrationTool/BJetCalibrationAlg.h>
#include <BJetCalibrationTool/BJetCalibrationSystAlg.h>

DECLARE_COMPONENT (BJetCalibrationTool)
DECLARE_COMPONENT (BJetCalibrationAlg)
DECLARE_COMPONENT (BJetCalibrationSystAlg)
