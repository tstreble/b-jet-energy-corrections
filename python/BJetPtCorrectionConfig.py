from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from AnalysisAlgorithmsConfig.ConfigBlock import ConfigBlock


class BJetPtCalibrationConfig(ConfigBlock):
    """
    Configuration for applying pt corrections to b-jets
    """

    def __init__(self, containerName):
        super(BJetPtCalibrationConfig, self).__init__()
        self.containerName = containerName
        self.addOption('muonQuality', 1, type=int)
        self.addOption('muonName', '', type=str)
        self.addOption('btagSelDecor', '', type=str)

    def makeAlgs(self, config):

        alg = config.createAlgorithm(
            'BJetCalibrationSystAlg',
            'BJetCalibAlg',
        )
        alg.muons = self.muonName
        alg.btagSelDecor = self.btagSelDecor
        alg.jets = config.readName(self.containerName)
        alg.jetsOut = config.copyName(self.containerName)

        config.addPrivateTool(
            'BJetCalibTool',
            'BJetCalibrationTool',
        )
        alg.BJetCalibTool.JetCollection = 'AntiKt4EMPFlow'

        config.addPrivateTool('BJetCalibTool.MuonSelectionTool', 'CP::MuonSelectionTool')
        alg.BJetCalibTool.MuonSelectionTool.MaxEta = 2.5
        alg.BJetCalibTool.MuonSelectionTool.MuQuality = self.muonQuality


def makeBJetPtCalibrationConfig(
    seq,
    containerName,
):
    config = BJetPtCalibrationConfig(containerName)
    seq.append(config)


def BJetCalibToolCfg(
    flags,
    # Medium, see Event/xAOD/xAODMuon/xAODMuon/versions/Muon_v1.h
    # in atlas/athena
    muonQuality=1,
    jetName='AntiKt4EMPFlow',
    **kwargs
):

    mst = CompFactory.CP.MuonSelectionTool(
        "MuonSelectionTool",
        MaxEta=2.5,
        MuQuality=muonQuality,
    )

    bjc = CompFactory.BJetCalibrationTool(
        "BJetCalibrationTool",
        JetCollection=jetName,
        MuonSelectionTool=mst,
        **kwargs
    )

    cfg = ComponentAccumulator()
    cfg.setPrivateTools(
        [mst, bjc]
    )
    return cfg


def makeBJetCalibAnalysisSequence(
    flags,
    jetSequence,
    muonName,
    # Name of decoration marking selected b-jets
    btagSelDecor,
    muonQuality=1,
    **kwargs
):

    # We don't pass the jet collection name here, because the tool
    # just wants the appropriate one for the derived corrections.
    # Assume we always use PFlow, which is the only one recommended
    toolcfg = BJetCalibToolCfg(flags, muonQuality, **kwargs)
    # Extract the tools and flag the tool CA as merged,
    # as not merging will cause an error
    tools = toolcfg.popPrivateTools()
    toolcfg._wasMerged = True

    bjc = tools[-1]
    alg = CompFactory.BJetCalibrationSystAlg(
        'BJetCalibAlg',
        muons=muonName,
        btagSelDecor=btagSelDecor,
        BJetCalibTool=bjc,
    )
    jetSequence.append(
        alg,
        inputPropName='jets',
        outputPropName='jetsOut',
        stageName='calibration'
    )


def BJetCalibSystAlgCfg(
    flags,
    jetName,
    muonName,
    # Name of decoration marking selected b-jets
    btagSelDecor,
    muonQuality=1,
    calibSuffix='bjetcalib',
    **kwargs
):

    cfg = ComponentAccumulator()
    # We don't pass the jet collection name here, because the tool
    # just wants the appropriate one for the derived corrections.
    # Assume we always use PFlow, which is the only one recommended
    toolcfg = BJetCalibToolCfg(flags, muonQuality, **kwargs)
    tools = cfg.popToolsAndMerge(toolcfg)
    bjc = tools[-1]
    alg = CompFactory.BJetCalibrationSystAlg(
        'BJetCalibAlg',
        jetsIn=jetName,
        jetsOut=f'{jetName}_{calibSuffix}',
        muonsIn=muonName,
        btagSelDecor=btagSelDecor,
        BJetCalibTool=bjc,
    )
    cfg.addEventAlgo(alg)

    return cfg
